package Day02_UserInput;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class day_02_BufferReader {
    public static void main(String[] args) throws IOException {
        int id;
        String name;
        float percentage;

        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the name");
        name=br.readLine();

        System.out.println("Enter the id");
        id=Integer.parseInt(br.readLine());

        System.out.println("Enter the percentage");
        percentage=Float.parseFloat(br.readLine());

        System.out.println("The name is :"+name);
        System.out.println("The id is :"+id);
        System.out.println("The percentage is :"+percentage);
           }
}
