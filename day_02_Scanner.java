package Day02_UserInput;

import java.util.Scanner;

public class day_02_Scanner {
    public static void main(String[] args) {
        int id;
        String name;
        float percentage;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter the id");
        id=scanner.nextInt();
        scanner.nextLine();
        System.out.println("The id is :"+id);

        System.out.println("Enter the name");
//        Scanner scanner1=new Scanner(System.in);
        name=scanner.nextLine();
        System.out.println("The name is :"+name);

        System.out.println("Enter the percentage");
        percentage=scanner.nextFloat();
        System.out.println("The percentage is :"+percentage);







    }
}
